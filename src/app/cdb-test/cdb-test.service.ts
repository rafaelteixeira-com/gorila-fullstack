import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { API_SERVER } from './../app.api';



@Injectable({ providedIn: 'root' })
export class CdbTestService {

    constructor(private http: HttpClient) {
    }

    // converte a data para o formato aceito no serviço
    getDateUS(date){
        const dateRef = new Date(date);
        return `${dateRef.getFullYear()}-${(dateRef.getMonth()+1)}-${dateRef.getDate()}`;
    }

    // end-point para calcular a progressão do CDB de acordo com a proposta do teste
    getCalc(calcData) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json'
            })
        };

        let params = new HttpParams()
            .set('investmentDate', this.getDateUS(calcData.investmentDate) )
            .set('cdbRate', calcData.cdbRate)
            .set('currentDate', this.getDateUS(calcData.currentDate)  );

        return this.http.get<any>(`${API_SERVER}/cdb/period`, {
            params : params
        }).pipe(map(data => {
                return data;
            })
        );
    }

}