import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CdbTestComponent } from './cdb-test.component';

export const routes: Routes = [
  {
    path: '',
    component: CdbTestComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CdbTestRoutingModule { 

}
