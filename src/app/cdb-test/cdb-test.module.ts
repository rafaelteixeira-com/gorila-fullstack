import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CdbTestComponent } from './cdb-test.component';
import { RouterModule } from '@angular/router';
import { routes } from './cdb-test-routing.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { MatSliderModule } from '@angular/material/slider';

import { NgxMaskModule } from 'ngx-mask';

import { SubloaderComponent } from './../subloader/subloader.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CdbTestComponent,
    SubloaderComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatSliderModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxMaskModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  providers: [  
    MatDatepickerModule,  
  ],
  bootstrap: [CdbTestComponent]
})
export class CdbTestModule { }
