# GorilaTest

Para rodar oteste deve-se usar o Node versão 12 ou superior;

Antes de rodar, lembre-se de instalar as dependencias do projeto (npm install);

### Rodar o teste localmente

Para rodar o back-end (serviço) e o fron-end (interface), execute o comando abaixo e aguarde alguns segundos para iniciar:

npm start

Acesse no browser: http://localhost:5000/

O serviço roda localmente: http://localhost:3333/

### Rodar o front separadamente

npm run frontend

### Rodar o back separadamente

npm run backend

### Para realizar o build da aplicação Angular

npm run build

### Para acessar no browser a aplicação:

http://localhost:5000/




### Instruções do Angular:

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0-next.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
