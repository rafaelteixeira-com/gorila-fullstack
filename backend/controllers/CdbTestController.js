
//metodos: index, show, update, store, destroy
/*
index: listagem de sessoes
store: Criar uma sessao
show: Quando queremos listar uma UNICA sessao
update: quando queremos alterar alguma sessao
destroy: quando queremos deletar uma sessao
*/ 

import Cdb from '../models/Cdb';

import CdbTestCalc from './CdbTestCalc';

class CdbTestController{


  async calcCdbPeriod(req, res){

    const cdbTestCalc = new CdbTestCalc();

    const { investmentDate, cdbRate, currentDate } = req.query;

    const resultDates = await Cdb.find({
      date: {
          $gte: new Date(investmentDate),
          $lt: new Date(currentDate)
      }
    }).sort({ date : 1 });

    const resultCalculed = cdbTestCalc.calc(resultDates, cdbRate);

    if(resultCalculed){
      return res.status(201).json({ resultCalculed });
    }
    else {
      return res.status(400).json({ 'msg':'Ocorreu um erro inesperado.' });
    }

  }

  async index(req, res){

    const result = await Cdb.find();
    return res.status(200).json({ result });

  }

  // carga dos dados da planilha via serviço
  async store(req, res){
    const { listCdb } = req.body;

    listCdb.map((item) => {
      //{"type":"CDI", "date":"12/3/2019", "price":4.9},
      item.date = new Date(item.date);
      return item
    });

    //console.log(listCdb);

    const result = await Cdb.create(listCdb);
    if(result){
      return res.status(201).json({ result });
    }
    else {
      return res.status(400).json({ 'msg':'Ocorreu um erro inesperado.' });
    }
  }

}

export default new CdbTestController();