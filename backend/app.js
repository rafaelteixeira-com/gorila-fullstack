import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import path from 'path';
import routes from './routes';
import bodyParser from 'body-parser';

class App{

  constructor(){
    this.server = express();
    /* */
    mongoose.connect('mongodb+srv://app-gorila:gorila2020@cluster0-qahtb.mongodb.net/test?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
   
    this.middlewares();
    this.routes();
  }

  middlewares(){
    /* */
    this.server.use(cors());
    this.server.use(bodyParser.json({limit: '50mb', extended: true}));
    this.server.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
    this.server.use(express.json());
  }

  routes(){
    this.server.use(routes);
  }

}

export default new App().server;
