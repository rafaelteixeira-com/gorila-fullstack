import { mongoose, Schema, model } from 'mongoose';
import Double from '@mongoosejs/double';
const SchemaTypes = Schema.Types;
const CdbSchema = new Schema({
  type: String,
  date: Date,
  price: SchemaTypes.Double,
});

export default model('Cdb', CdbSchema);