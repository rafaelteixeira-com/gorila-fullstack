import { Router } from 'express';

import CdbTestController from './controllers/CdbTestController';

const routes = new Router();

routes.get('/cdb/period', CdbTestController.calcCdbPeriod);

routes.get('/cdb/list', CdbTestController.index);

routes.post('/cdb', CdbTestController.store);

export default routes;